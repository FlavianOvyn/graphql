# @Ovyn/Graphql

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.1.0.

## Usage
### Configuration
1. Module import
````javascript
GraphqlModule
````
2. Factory configuration
- Store
````javascript
    function graphqlStoreFactory(
      $store: Store,
      selector: MemoizedSelector<any, any>,
      dispatch: () => TypedAction<any>,
      map: OperatorFunction<any, any>
    ) {
      $store.dispatch(dispatch())
      return $store.select(selector)
        .pipe(
          map
        )
    }
````
- Value
````javascript
    function graphqlFactory(): Observable<GraphqlConfig> {
        return of ({ endpoint: "http://localhost:3000/graphql "})
    }
````

3. Service configuration
- GRAPHQL_CONFIG provider
````javascript
{
  provide: GRAPHQL_CONFIG,
  useFactory: ($store: Store) => graphqlStoreFactory(
  $store,
  configBaseUriSelector,
  ConfigActions.init,
  map(baseUrl => ({endpoint: `${baseUrl}/graphql`}))
),
  deps: [Store]
}
````
- GraphqlService provider
````javascript
{
    provide: GraphqlService,
    useFactory: ($config: Observable<GraphqlConfig>, $http: HttpClient) => new GraphqlService($config, $http),
    deps: [GRAPHQL_CONFIG, HttpClient]
}
````
