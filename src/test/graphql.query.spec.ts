import {GraphqlType, Parameter, query} from "../lib";

describe('QueryBuilder', () => {

  it('query should throw Error("Query must have fields")', function () {
    const builder = query("test")

    expect(() => builder.build()).toThrow(new Error("Query must have fields"))
  });

  it('queryField should throw Error("Query must have fields")', function () {
    const builder = query("test").addQueryField("q1")

    expect(() => builder.build()).toThrow(new Error("Query must have fields"))
  });

  it('should request field id', function() {
    const builder = query("test").addField("id")

    expect(builder.build()).toEqual({operationName: "Test", query: 'query Test{ test{ id } }', variables: {}})
  })

  it('should request query q1', function () {
    const builder = query("test")
      .addQueryField("q1").addField("field1")

    expect(builder.build()).toEqual({operationName: "Test", query: "query Test{ test{ q1{ field1 } } }", variables: {}})
  })

  it('should request with parameters', function() {
    const params = new Map<string, Parameter>()
    params.set("p1", { name: 'id', type: GraphqlType.M_ID, value: 1})
    const builder = query("test", params).addField("field1");

    expect(builder.build()).toEqual({operationName: "Test", query: "query Test($p1: ID!){ test(id: $p1){ field1 } }", variables: {p1: '1'}})
  })

  it('should request with two query q1 and q2', function () {
    const builder = query("test")
      .addQueryField("q1").addField("field11").and()
      .addQueryField("q2").addField("field21")

    expect(builder.build()).toEqual({operationName: "Test", query: "query Test{ test{ q1{ field11 }, q2{ field21 } } }", variables: {}})
  })

  it('should request with two parametered queries', function () {
    const q1Params = new Map<string, Parameter>()
    q1Params.set('q1param', { name: 'p1', type: GraphqlType.M_ID, value: 1})
    const q2Params = new Map<string, Parameter>()
    q2Params.set('q2param', { name: 'p1', type: GraphqlType.M_ID, value: 1})
    const builder = query("test")
      .addQueryField("q1", q1Params).addField("field11").and()
      .addQueryField("q2", q2Params).addField("field21").and()
      .addField("blop")

    expect(builder.build()).toEqual({operationName: "Test", query: "query Test($q1param: ID!, $q2param: ID!){ test{ q1(p1: $q1param){ field11 }, q2(p1: $q2param){ field21 }, blop } }", variables: {q1param: '1', q2param: '1'}})
  })

})
