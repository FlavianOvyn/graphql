import {GraphqlType, Parameter} from "../graphql.model";

export function parameterDefinition(params: Map<string, Parameter>): string {
  const str: string[] = ["("]
  let index = 0

  for(const [variable, parameter] of params) {
    str.push(`\$${variable}`, ': ', parameter.type)
    if (++index < params.size) {
      str.push(",", " ")
    }
  }

  str.push(")")

  return str.join("")
}

export function parameterCreator(parameters: Map<string, Parameter>): { parameters: string, variables: any } {
  const str: string[] = ["("]
  const variables: any = {}
  let index = 0

  for(const [variable, parameter] of parameters) {
    str.push(`${parameter.name}: \$${variable}`)
    variables[`${variable}`] = transformToGraphql(parameter)
    if (++index < parameters.size) {
      str.push(",", " ")
    }
  }

  str.push(")")
  return { parameters: str.join(""), variables }
}
export function transformToGraphql(parameter: Parameter): any {
  let value: any

  switch (parameter.type) {
    case GraphqlType.ID:
    case GraphqlType.M_ID:
    case GraphqlType.String:
    case GraphqlType.M_String:
      value = `${parameter.value}`
      break
    case GraphqlType.Int:
    case GraphqlType.M_Int:
    case GraphqlType.Float:
    case GraphqlType.M_Float:
    case GraphqlType.Boolean:
    case GraphqlType.M_Boolean:
      value = parameter.value
      break
  }

  return value
}
