import {Gql, GraphqlMode, Parameter} from "../graphql.model";
import {transformToGraphql} from "./parameter.util";
// import '../string.extension'

declare global {
  interface String {
    capitalize(): string
  }
}

String.prototype.capitalize = function () {
  return [this[0].toUpperCase(), ...this.substring(1)].join("")
}

class Builder {
  private parent?: Builder
  private parameters = new Map<string, Parameter>()
  private varDeclaration = new Map<string, Parameter>
  private fields: Builder[] = []
  private type: GraphqlMode = GraphqlMode.query


  constructor(private name: string, parameters?: Map<string, Parameter>) {
    parameters?.forEach((v,k) => {
      this.parameters.set(k,v)
      this.varDeclaration.set(k,v)
    })
  }

  addField(...names: string[]) {
    names.forEach(name => {
      const builder = new Builder(name)
      builder.parent = this
      builder.type = GraphqlMode.field
      this.fields.push(builder)
    })
    return this
  }
  addQueryField(name: string, parameters?: Map<string, Parameter>) {
    const builder = new Builder(name)
    builder.parent = this
    builder.type = GraphqlMode.query

    parameters?.forEach((v,k) => {
      builder.parameters.set(k,v)
      if (this.parent) {
        this.getTopAncestor().varDeclaration.set(k,v)
      } else {
        this.varDeclaration.set(k,v)
      }
    })

    this.fields.push(builder)

    return builder
  }

  and() { return this.parent! }

  private getTopAncestor() {
    let builder: Builder = this

    while (!!builder.parent) {
      builder = builder.parent
    }

    return builder
  }

  build(): Gql {
    const builder = this.getTopAncestor() || this;
    const query = builder.createQuery()
    const variables: any = {}
    const operationName = builder.name.capitalize()
    this.varDeclaration.forEach((v,k) => variables[k] = transformToGraphql(v))

    return { operationName, query, variables }
  }

  private createQuery() {
    if (this.type === GraphqlMode.query && this.fields.length == 0) throw new Error("Query must have fields")
    const str: string[] = []
    if (!this.parent) {
      str.push(`query ${this.name.capitalize()}`)
      if (this.varDeclaration.size > 0) {
        str.push(this.createVarDeclaration())
      }
      str.push("{ ")
    }
    str.push(this.name)
    if (this.parameters.size > 0) {
      str.push(this.createParameterStr())
    }
    if (this.fields.length > 0) {
      str.push("{ ")
      for(const [i, field] of this.fields.entries()) {
        str.push(field.createQuery())
        if (i + 1 < this.fields.length) {
          str.push(", ")
        }
      }
      str.push(" }")
    }

    if (!this.parent) {
      str.push(" }")
    }

    return str.join("")
  }

  private createVarDeclaration() {
    const str: string[] = []

    str.push(`(`)
    let index = 0
    const nb = [...this.varDeclaration.keys()].length
    this.varDeclaration.forEach((v,k) => {
      str.push(`\$${k}: ${v.type}`)
      if (++index < nb) {
        str.push(", ")
      }
    })
    str.push(")")

    return str.join("")
  }
  private createParameterStr() {
    const str: string[] = []
    let index = 0;
    const nb = [...this.parameters.keys()].length
    str.push(`(`)
    this.parameters.forEach((v,k) => {
      str.push(`${v.name}: \$${k}`)
      if(++index < nb) {
        str.push(", ")
      }
    })
    str.push(")")

    return str.join("")
  }
}


export function query(name: string, parameters?: Map<string, Parameter>) {
  const builder = new Builder(name, parameters)

  return builder;
}


