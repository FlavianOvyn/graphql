import {inject, Inject, Injectable, InjectionToken} from '@angular/core';
import {GRAPHQL_CONFIG} from "./graphql.module";
import {Gql, GraphqlConfig, GraphqlResponse} from "./graphql.model";
import {catchError, map, Observable, of, switchMap, tap} from "rxjs";
import {HttpClient} from "@angular/common/http";

export class GraphqlService {

  constructor(
    private readonly $config: Observable<any>,
    private readonly $http: HttpClient
  ) {
  }

  execute<T>(gql: Gql): Observable<T> {
    return this.$config.pipe(
      tap(data => console.log(data)),
      switchMap(({endpoint}) => this.$http.post<GraphqlResponse<T>>(`${endpoint}`, {...gql}) ),
      map(it => it.data),
    )
  }
}
