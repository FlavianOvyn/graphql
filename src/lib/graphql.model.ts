export type GraphqlConfig = { endpoint: string }
export type GraphqlResponse<T> = { data: T }

export type Gql = { operationName: string, query: string, variables?: any }
export type Parameter = { name: string, type: GraphqlType, value: any }

export enum GraphqlMode {
  query= 'query',
  mutation= 'mutation',
  field= 'field'
}

export enum GraphqlType {
  ID= 'ID',
  M_ID= 'ID!',
  Int= 'Int',
  M_Int= 'Int!',
  Float= 'Float',
  M_Float= 'Float!',
  String= 'String',
  M_String= 'String!',
  Boolean= 'Boolean|Boolean!',
  M_Boolean= 'Boolean!'
}
