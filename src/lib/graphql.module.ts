import {EnvironmentProviders, InjectionToken, ModuleWithProviders, NgModule, Provider} from '@angular/core';
import {GraphqlService} from "./graphql.service";
import {GraphqlConfig} from "./graphql.model";
import {Observable} from "rxjs";
// import {storeFactory} from "./factories";
import {HttpClient} from "@angular/common/http";

export const GRAPHQL_CONFIG = new InjectionToken<() => Observable<GraphqlConfig>>("Graphql.endpoint")


@NgModule({
  declarations: [],
  imports: [
  ],
  providers: []
})
export class GraphqlModule {

  // static forRoot(opt: any): ModuleWithProviders<GraphqlModule> {
  //   const providers: (Provider | EnvironmentProviders)[]  = []
  //
  //   if (opt.store) {
  //     const factory = opt.factory
  //     providers.push(
  //       {
  //         provide: GRAPHQL_CONFIG,
  //         useFactory: ($store: any) => factory($store),
  //         deps: []
  //       },
  //       {
  //         provide: GraphqlService,
  //         useFactory: ($gqlConfig: Observable<GraphqlConfig>, $http: HttpClient) => new GraphqlService($gqlConfig, $http),
  //         deps: [GRAPHQL_CONFIG, HttpClient]
  //       }
  //     )
  //   }
  //
  //   return {
  //     ngModule: GraphqlModule,
  //     providers
  //   }
  // }
}
